# NixOS Configuration

This repository showcases my NixOS dotfiles, tailored to enhance your system configuration.

When `workstation.enable` is set to `true`, the configuration includes:

- a thoughtfully curated collection of modern GNOME applications
- seamless setup for printers and scanners
- integration of Bluetooth and Pipewire for optimal connectivity
- elevated Wayland support for an enhanced graphical experience
- essential applications like Firefox and MPV for everyday tasks
- ...

Conversely, when `workstation.enable` is set to `false`, the configuration streamlines to a minimalist setup.

## How to install

Follow https://openzfs.github.io/openzfs-docs/Getting%20Started/NixOS/Root%20on%20ZFS.html. When cloning the template configuration,
instead of running:

```
mkdir -p "${MNT}"/etc
git clone --depth 1 --branch openzfs-guide \
  https://github.com/ne9z/dotfiles-flake.git "${MNT}"/etc/nixos
```

Run:

```
mkdir -p "${MNT}"/etc
git clone --depth 1 --branch main \
  https://gitlab.com/mplavsic/nixos-configuration "${MNT}"/etc/nixos
```

Some adjustments need to be done to successfully follow the guide.

### Choose exampleHost host

Remove the following line in `flake.nix`:

```
the-world = mkHost "the-world" "x86_64-linux" "23.11" "manuel" "Manuel";
```

and uncomment:

```
# exampleHost = mkHost "exampleHost" "x86_64-linux" "23.11" "userName" "userDescription";
```

Feel free to change the username and user description. However, it is easiest to keep the host name like this for the moment, i.e., *exampleHost*.

### Remove the-world host

Since this host has a hardware configuration matching my computer, it would sense only for me to keep it (when reinstalling). Thus, you can remove the directory `hosts/the-world`.

### Copy root hash to host-specific configuration.nix

In *System configuration - Part 4*, instead of running:

```
sed -i \
"s|rootHash_placeholder|${rootPwd}|" \
"${MNT}"/etc/nixos/configuration.nix
```

Run:

```
sed -i \
"s|rootHash_placeholder|${rootPwd}|" \
"${MNT}"/etc/nixos/CORRECT_HOST_NAME_GOES_HERE/configuration.nix
```

NB: Make sure you enter the host name (e.g. `the-world`) and **not** the username (e.g. `manuel`).

## Post installation

### Change host name

To change host name, follow the steps in `hosts/exampleHost/changeHostName.txt`.

### Unlock login keyring with autologin enabled

Do not use `services.gnome.gnome-keyring.enable = lib.mkForce false;`. It may seem to work correctly at first,
but some apps need gnome keyring, especially when using the GNOME DE. It might become a problem if that option
is set to false.

Instead, what one needs to do is the following:

1. open Passwords and Keys (formally Seahorse)
2. right click on login under passwords
3. click change password, type the old password in. The new password has to be the empty string.

Now the keyring is unencrypted. It is fine as long as the device was set up with full-disk encryption.

Source: https://www.reddit.com/r/pop_os/comments/if3kgf/autologin_authentication_required/

### Registry: same revision for both config and nix commands

In the top-level `configuration.nix`, the following configuration is present:

```
nix.registry.nixpkgs.to = { type = "github"; owner = "NixOS"; repo = "nixpkgs"; ref = nixpkgs.rev; };
```

This ensures that the specified system-wide registry is used not only for packages specified in the config, but also when using
`nix shell` and other *experimental*/new nix commands.

**NB:** legacy commands such as `nix-shell -p sqlite` do not leverage the nixpkgs registry above.
Only commands such as `nix shell nixpkgs#sqlite` work with it correctly.


#### Check configuration

It is possible to tell if the setup with the registry is working by looking at
the output of `nix registry list`:
there should be a 'system' pin with a fixed revision. This will make the
current system's revision the default whenever using `nixpkgs`.

NB: Without such configuration, there is some logic which dictates what
'nixpkgs' means at a given moment in time, which includes fetching
the latest version occasionally AFAIR.

#### Multiple packages

To download multiple packages:
- `nix shell nixpkgs#sqlite nixpkgs#sqlite.dev`
- `nix shell nixpkgs#{sqlite,sqlite.dev})`
  - NB: no empty space allowed such as in `nix shell nixpkgs#{sqlite,  sqlite.dev})`
- `nix shell nixpkgs#sqlite{,.dev}`
  - NB: no empty space allowed such as in `nix shell nixpkgs#sqlite{, .dev}`

#### Allow with legacy commands

Similar results can be achieved by setting nix.nixPath to `[]"${nixpkgs}"]`.
Setting `NIX_PATH` also affects the legacy packages, however, the main reason not to use
it is that environment variable updates require a logut/logout.

This solution would replace 

```
nix.registry.nixpkgs.to = { type = "github"; owner = "NixOS"; repo = "nixpkgs"; ref = nixpkgs.rev; };
```

#### etc symlink

Right below

```
nix.registry.nixpkgs.to = { type = "github"; owner = "NixOS"; repo = "nixpkgs"; ref = nixpkgs.rev; };
```

we find:

```
environment.etc.nixpkgs-source.source = nixpkgs;
```

The intent of this is to have a symlink in etc which points at the nixpkgs,
which can be useful for example if grepping it is wanted.
And also to make sure that the nixpkgs flake itself is live as
a garbage collector root to avoid having to redownload it
(but I'm not certain this implementation is sound).

# Original Readme

Open README.org