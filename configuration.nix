# Configuration in this file is shared by all hosts, servers and workstations.

{ pkgs, dbPath, inputs, lib, stateVersion, config, userName, userDescription, nixpkgs, hostName, ... }: {

  # ------------------------------------------
  # UNFREE AND STATEVERSION
  # ------------------------------------------

  # Necessary to enable unfree hardware firmware
  # it also allows workspace-specific and host-specific unfree packages
  nixpkgs.config.allowUnfree = true;

  # in case of reinstall, make sure that system.stateVersion is set to something meaningful
  system.stateVersion = stateVersion; # NB: unstable sets always to the next version (e.g. set to 23.11 if it is August 2023)
  # NB: setting this config per host may cause package configs across different systems.

  # NB: Read README.md for the next two configs
  nix.registry.nixpkgs.to = { type = "github"; owner = "NixOS"; repo = "nixpkgs"; ref = nixpkgs.rev; };
  environment.etc.nixpkgs-source.source = nixpkgs;

  # ------------------------------------------
  # KEYBOARD
  # ------------------------------------------

  # https://www.farah.cl/Keyboardery/A-Visual-Comparison-of-Different-National-Layouts/
  # NB: Swiss keyboards are usually ISO.
  # On the weppage above, US ISO (terminal) is the layout that is adopted on Swiss keyoards, by default.

  # # Configure keymap in X11
  services.xserver = {
    # xkbOptions = "altwin:ctrl_alt_win,caps:swapescape,compose:ralt,terminate:ctrl_alt_bksp,lv3:lsgt_switch";
    xkbOptions = "compose:ralt";
    # # layout = "ch";
    # # xkbVariant = "sg"; # probably not needed if layout "ch" is selected
  };
  # # Configure console keymap
  console = {
    useXkbConfig = true;
    # console.keyMap = "sg"; # probably not needed if console.useKbcConfig is true
  };

  # NB: if you enable services.xserver.xkbOptions and/or console.useXkbConfig, then you need to run:
  # gsettings reset org.gnome.desktop.input-sources sources
  # gsettings reset org.gnome.desktop.input-sources xkb-options

  # ------------------------------------------
  # LOCALE
  # ------------------------------------------

  # Select internationalisation properties.
  i18n.defaultLocale = "en_IE.UTF-8";

  # In case some of the locale settings need to be overriden.
  # i18n.extraLocaleSettings = {
  #   LANGUAGE = "en_IE.UTF-8";
  #   LC_ALL = "en_IE.UTF-8";
  #   LC_ADDRESS = "en_IE.UTF-8";
  #   LC_IDENTIFICATION = "en_IE.UTF-8";
  #   LC_MEASUREMENT = "en_IE.UTF-8";
  #   LC_MONETARY = "en_IE.UTF-8";
  #   LC_NAME = "en_IE.UTF-8";
  #   LC_NUMERIC = "en_IE.UTF-8";
  #   LC_PAPER = "en_IE.UTF-8";
  #   LC_TELEPHONE = "en_IE.UTF-8";
  #   LC_TIME = "en_IE.UTF-8";
  # };

  # ------------------------------------------
  # HARDWARE, FILESYSTEMS AND NETWORKING
  # ------------------------------------------

  imports = [
    # Enables non-free firmware on devices not recognized by `nixos-generate-config`
    "${inputs.nixpkgs}/nixos/modules/installer/scan/not-detected.nix"

    # "${nixpkgs}/nixos/modules/profiles/hardened.nix"
    # "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
  ];

  # Enable NetworkManager for wireless networking,
  # You can configure networking with "nmtui" command.
  networking.useDHCP = false;
  networking.networkmanager.enable = true;

  services.openssh = {
    enable = lib.mkDefault true;
    settings = { PasswordAuthentication = false; };
  };

  # Quad9 - DNS over TLS (disabled in favor of DNSCrypt (below))
  # networking.nameservers = [ "2620:fe::fe" "2620:fe::9" "9.9.9.9" "149.112.112.112"];
  # # Let's configure systemd-resolved to use DNS over TLS, DNSSEC.
  # services.resolved.enable = true;
  # services.resolved.dnssec = "true";
  # services.resolved.extraConfig = ''
  #   DNSOverTLS=yes
  # '';

  # Quad9 - DNSCrypt
  # ! NB: Make sure /etc/resolv.conf is deleted
  networking = {
    nameservers = [ "127.0.0.1" "::1" ];
    resolvconf.enable = pkgs.lib.mkForce false;
    dhcpcd.extraConfig = "nohook resolv.conf";
  };
  networking.networkmanager.dns = "none";
  services = { 
    resolved.enable = false;
    dnscrypt-proxy2 = {
      enable = true;
      settings = {
        listen_addresses = [ "127.0.0.1:53000" "[::1]:53000" ];
        ipv4_servers = true;
        ipv6_servers = true;
        require_dnssec = true;
        require_nolog = true;

        sources.public-resolvers = {
          urls = [
            "https://quad9.net/dnscrypt/quad9-resolvers-dnscrypt.md"
            "https://raw.githubusercontent.com/Quad9DNS/dnscrypt-settings/main/dnscrypt/quad9-resolvers-dnscrypt.md"
          ];
          cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
          minisign_key = "RWQBphd2+f6eiAqBsvDZEBXBGHQBJfeG6G+wJPPKxCZMoEQYpmoysKUN";
        };
        server_names = [ "dnscrypt-ip6-filter-alt" "dnscrypt-ip6-filter-alt2" "dnscrypt-ip6-filter-pri" "dnscrypt-ip4-filter-alt" "dnscrypt-ip4-filter-alt2" "dnscrypt-ip4-filter-pri" ];
        # These server names are for high security / high privacy.
        # See more options here https://en.wikipedia.org/wiki/Quad9.
      };
    };
  };

  # Forward loopback traffic on port 53 to dnscrypt-proxy2.
  networking.firewall.extraCommands = ''
    iptables --table nat --flush OUTPUT
    ${lib.flip (lib.concatMapStringsSep "\n") [ "udp" "tcp" ] (proto: ''
      iptables --table nat --append OUTPUT \
        --protocol ${proto} --destination 127.0.0.1 --destination-port 53 \
        --jump REDIRECT --to-ports 53000
    '')}
  '';

  # TODO: dnsmasq.conf works, however resolv.conf needs to be tested
  systemd.tmpfiles.rules = [
    "L+ /etc/dnsmasq.conf - - - - /etc/nixos/dns/dnsmasq.conf"
    "L+ /etc/resolv.conf - - - - /etc/nixos/dns/resolv.conf"
  ] # not finished, list concatenation below
  
  # ------------------------------------------
  # USERS AND SECURITY
  # ------------------------------------------
  # (the following creates an `/unsynced` folder, for content that should not be included in zfs snapshots).
  ++ [
    "d /unsynced 0755 root root -"
    "d /unsynced/${userName} 0700 ${userName} users -"
    # NB: In case a second user is added, manually add an /unsynced/userName folder
    # second user example: "d /unsynced/alice 0700 alice users -"
  ];

  users = {
    mutableUsers = false;
    users = {
      ${userName} = {
        inherit (config.users.users.root) initialHashedPassword;
        isNormalUser = true;
        description = userDescription;
        extraGroups = [ "networkmanager" "wheel" ];
      };
    };
  };

  security = {
    doas = {
      enable = true;
      extraRules = [{
        users = [ userName ];
        persist = true;
      }];
    };
    sudo.enable = lib.mkDefault false;
  };

  # ------------------------------------------
  # ENVIRONMENTS AND PROGRAMS
  # ------------------------------------------

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  programs.direnv.enable = true;

  # More info on this approach:
  # https://blog.nobbz.dev/2023-02-27-nixos-flakes-command-not-found/ ("Use the flake-programs-sqlite-flake" part.
  programs.command-not-found = {
    enable = true;
    dbPath = dbPath;
  };

  environment.sessionVariables = let unsyncedPath = "/unsynced/$USER"; in { 
    UNSYNCED=unsyncedPath;
    XDG_DOWNLOAD_DIR="${unsyncedPath}/Downloads";
    XDG_CACHE_HOME="${unsyncedPath}/.cache";
    # Do not set `XDG_CONFIG_HOME="$UNSYNCED/.config";`,
    # since Signal and other applications store their data
    # in ~/.config, which needs to be backed up alongside
    # the rest of $HOME
    NPM_CACHE_LOCATION="${unsyncedPath}/.npm";
    GRADLE_USER_HOME="${unsyncedPath}/.gradle";
    PUB_CACHE="${unsyncedPath}/.pub-cache";
    ANALYZER_STATE_LOCATION_OVERRIDE="${unsyncedPath}/.dartServer";
    COMPOSER_HOME="${unsyncedPath}/.composer";
    # for php also see https://stackoverflow.com/questions/33080068/running-composer-in-a-different-directory-than-current
    # maybe set envar for rust cache too (in case sccache is used)
  };

  programs.git.enable = true;

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting "🐟"
    '';
    shellAliases = {
      restic-service-restart = "systemctl restart restic-backups-${hostName}.service";
      restic-service-status = "systemctl status restic-backups-${hostName}.service";
    };
  };

  programs.starship = {
    enable = true;
  };

  users.defaultUserShell = pkgs.fish;

  environment.systemPackages = builtins.attrValues {
    inherit (pkgs)
      dig
      helix
      jq
      starship
      nushell
    ;
  };

  # NB: "hx" denotes the executable binary of Helix, a modern text editor written in Rust.
  environment.variables = {
    SUDO_EDITOR = "hx";
    VISUAL = "hx";
    EDITOR = "hx";
  };

  # Neovim is installed as fallback text editor.
  programs.neovim = {
    enable = true;
    defaultEditor = false;
  };

}
