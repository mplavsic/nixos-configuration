#           ▜███▙       ▜███▙  ▟███▛
#            ▜███▙       ▜███▙▟███▛
#             ▜███▙       ▜██████▛
#      ▟█████████████████▙ ▜████▛     ▟▙
#     ▟███████████████████▙ ▜███▙    ▟██▙
#            ▄▄▄▄▖           ▜███▙  ▟███▛
#           ▟███▛             ▜██▛ ▟███▛
#          ▟███▛               ▜▛ ▟███▛
# ▟███████████▛                  ▟██████████▙
# ▜██████████▛                  ▟███████████▛
#       ▟███▛ ▟▙               ▟███▛
#      ▟███▛ ▟██▙             ▟███▛
#     ▟███▛  ▜███▙           ▝▀▀▀▀
#     ▜██▛    ▜███▙ ▜██████████████████▛
#      ▜▛     ▟████▙ ▜████████████████▛
#            ▟██████▙       ▜███▙
#           ▟███▛▜███▙       ▜███▙
#          ▟███▛  ▜███▙       ▜███▙
#          ▝▀▀▀    ▀▀▀▀▘       ▀▀▀▘
{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    
    # programsdb is used to enable command-not-found with flakes (maybe needed cause of fish)
    programsdb = {
      url = "github:wamserma/flake-programs-sqlite";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      # if url does not work:
      # url = "github:nix-community/home-manager/release-23.11"; # at the time of writing (Aug, 2023), unstable is 23.11
    };
  };

  outputs = { self, nixpkgs, programsdb, home-manager }@inputs:
  # The @inputs ensures inputs can be used in the code below (in `specialArgs` to be precise)
    let
      mkHost = hostName: system: stateVersion: userName: userDescription:
        (({ workstation, zfs-root, pkgs, system, ... }:
          nixpkgs.lib.nixosSystem {
            inherit system;

            # List of args that don't have to be passed down to NixOS modules explicitly.
            # This way, `modules` can look like:
            # 
            # ```nix
            # modules = [
            #   ./configuration.nix
            # ]
            # ```
            #
            # instead of
            #
            # ```nix
            # modules = [
            #   (import ./configuration.nix { inherit pkgs dbPath inputs; })
            # ]
            # ```
            #
            # ! NB: this ensures, e.g., allowUnfree is correctly recognized.
            specialArgs = {
              hostName = hostName;
              userName = userName;
              userDescription = userDescription;

              # System special args (mandatory):
              workstation = workstation;
              zfs-root = zfs-root;
              # ! Enable or disabling the following two doesn't seem to alter configuration:
              lib = nixpkgs.lib;
              system = system;
              # ! Uncommenting the next line would break system-wide unfree config:
              # pkgs = pkgs; # NB: leave it commented out
              inherit nixpkgs;

              # Other inputs:
              inherit inputs;

              stateVersion = stateVersion;

              # As part of command-not-found workaround:
              dbPath = programsdb.packages.${pkgs.system}.programs-sqlite;
            };

            modules = [

              # Module 1: config shared by all hosts
              ./configuration.nix

              # Module 2: zfs-root and workstation
              ./modules

              # Module 3: entry point
              ({ workstation, zfs-root, pkgs, lib, ... }: {
                inherit workstation zfs-root;
              })

              # Module 4: host-specific config, if it exist
              (if (builtins.pathExists ./hosts/${hostName}/configuration.nix)
                then ./hosts/${hostName}/configuration.nix 
                else {}
              )
              
              # Module 5: home manager
              home-manager.nixosModules.home-manager
              # NB: home manager attrset is in modules/workstation/default.nix

            ];
          })

        # configuration input
          (import ./hosts/${hostName} {
            system = system;
            pkgs = nixpkgs.legacyPackages.${system};
          }));
    in {
      nixosConfigurations = {
        the-world = mkHost "the-world" "x86_64-linux" "23.11" "manuel" "Manuel";
        the-moon = mkHost "the-moon" "x86_64-linux" "23.11" "manuel" "Manuel";
      };
    };
}
