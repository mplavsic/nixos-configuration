# configuration in this file only applies to exampleHost host
#
# only workstation.* and zfs-root.* options can/should be defined in this file.
#
# all others goes to `configuration.nix` under the same directory as
# this file. 

{ system, pkgs, ... }: {
  inherit pkgs system;
  zfs-root = {
    boot = {
      devNodes = "/dev/disk/by-id/";
      bootDevices = [ "bootDevices_placeholder" ];
      immutable = false;
      availableKernelModules = [ "kernelModules_placeholder" ];
      removableEfi = false; # NB: this is true in official guide. Having false here might be harmful on some devices.
      kernelParams = [ ];
      sshUnlock = {
        # read sshUnlock.txt file.
        enable = false;
        authorizedKeys = [ ];
      };
      disableSwap = false; # if true, the swap partition does not have to exist
    };
    networking = {
      # read changeHostName.txt file.
      hostName = "exampleHost";
      timeZone = "Europe/Zurich";
      hostId = "abcd1234";
    };
  };

  workstation.enable = true; # NB:
  # - False implies a minimal selection of programs that is ideal for servers.
  # - Set to true if the device is an end user device, e.g., a laptop.
}
