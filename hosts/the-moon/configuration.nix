{ pkgs, ... }: {
  # configuration in this file only applies to the particular host.

  # boot.zfs.forceImportRoot = false; # maybe set to true

  # in case of reinstall, make sure that system.stateVersion is set to something meaningful
  system.stateVersion = "23.11"; # NB: unstable sets always to the next version (e.g. set to 23.11 if it is August 2023)
  # NB: setting this config per host may cause package configs across different systems.

  users.users.root = {
    initialHashedPassword = "rootHash_placeholder";
    openssh.authorizedKeys.keys = [ "sshKey_placeholder" ];
  };
}