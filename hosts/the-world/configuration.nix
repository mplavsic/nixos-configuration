{ pkgs, userName, ... }: {
  # configuration in this file only applies to the particular host.

  # boot.zfs.forceImportRoot = false; # maybe set to true

  users.users.root = {
    initialHashedPassword = "$6$YEsgrRqc7kupT/no$AIHpeDHKlzhdlW8VQMfqssYyKLVETPOY.d0vUrLWMzrUFC8c5th2EvFOSrM/vBLufzWCa1e9uK0LzBXFWCnCp.";
    openssh.authorizedKeys.keys = [ "sshKey_placeholder" ];
  };

  # Disable this due to error:
  #
  # ```
  # setting up tmpfiles
  # Failed to open file "/sys/class/drm/card0/device/power_dpm_force_performance_level": Permission denied
  # ```
  #
  # systemd.tmpfiles.rules = [
  #   "w /sys/class/drm/card0/device/power_dpm_force_performance_level 0444 - - - high"
  # ];

  # In order to diable power save mode, the following might be needed:
  #
  # systemd.tmpfiles.rules = [
  #   "w /sys/module/snd_hda_intel/parameters/power_save 0444 - - - 0"
  # ];

  services.zfs.expandOnBoot = "all";

  boot.blacklistedKernelModules = [ "mtk_t7xx" ];

  services.restic.backups.the-world = {
    initialize = false; # manually and temporarily set to true only first time
    user = userName; # root is probably not needed
    environmentFile = "/etc/nixos/hosts/the-world/restic-environment-file";
    repositoryFile = "/etc/nixos/hosts/the-world/restic-repository-file";
    passwordFile = "/etc/nixos/hosts/the-world/restic-password-file";
    pruneOpts = [
      "--keep-daily 7"
      "--keep-weekly 4"
      "--keep-monthly 2"
      "--keep-yearly 0"
    ];
    paths = [
      "/home/${userName}"
      "/unsynced/${userName}/Workspace" # uncommited changes should be backed up
      "/etc/nixos" # uncommited changes should be backed up
    ];
    exclude = [
      "/unsynced/${userName}/Workspace/**/build" # Dart/Flutter
      "/unsynced/${userName}/Workspace/**/.dart_tool" # Dart/Flutter
      "/unsynced/${userName}/Workspace/**/target" # Rust (even though "--exclude-caches" should be enough)
      "/home/${userName}/.cache"
      "/home/${userName}/.local/share/waydroid" # permission issues with normal user
      "/home/${userName}/.local/share/Trash"
      "/home/${userName}/.var/app/*/cache"
      "/home/.zfs" # zfs snapshots should not be backed up
    ];
    extraBackupArgs = [
      "--exclude-caches"
    ];
    timerConfig = {
      OnCalendar = "hourly";  # Empty string to disable the timer
      Persistent = true;
      # NB: the option Persistent=true triggers the service
      # immediately if it missed the last start time
    };
  };

  # NB: in case of multiple devices to back up, this part
  # could be added to workspace
  services.sanoid = {
    enable = true;
    extraArgs = [
      "--verbose"
    ];
    interval = "hourly";
    datasets."rpool/nixos/home" = {
      recursive = false; # does not matter much in my case, as there are no dataset children
      autosnap = true;
      autoprune = true;
      daily = 7;
      weekly = 4;
      monthly = 2;
      yearly = 0;
    };
  };
  #! NB: common zfs commands (in case i don't manually run them in a long time)

  programs.fish = {
    shellAliases = {
      backupToExternalDrive = "doas zpool import backup && doas syncoid --sendoptions=\"w\" --compress=none --delete-target-snapshots --no-sync-snap --no-rollback rpool/nixos/home backup/eds && doas zpool export backup && udisksctl power-off -b /dev/disk/by-id/usb-Samsung_PSSD_T7_S6XNNS0TB03264T-0:0";
      # NB: in case more flexibility is needed, the below aliases can be enabled
      # backupImportDataset = "doas zpool import backup";
      # backupRunSyncoid = "doas syncoid --compress=zstd-fast --delete-target-snapshots rpool/nixos/home backup/eds";
      # backupExportDataset = "doas zpool export backup";
      # backupPowerOffDrive = "udisksctl power-off -b /dev/disk/by-id/usb-Samsung_PSSD_T7_S6XNNS0TB03264T-0:0";
      connect-waydroid="adb connect 192.168.240.112:5555";
    };
  };

}
