# configuration in this file only applies to the-world host
#
# only workstation.* and zfs-root.* options can/should be defined in this file.
#
# all others goes to `configuration.nix` under the same directory as
# this file. 

{ system, pkgs, ... }: {
  inherit pkgs system;
  zfs-root = {
    boot = {
      devNodes = "/dev/disk/by-id/";
      bootDevices = [ "nvme-MTFDKBA1T0TFH-1BC1AABHA_UMDMD0153HYDP9" ];
      immutable = false;
      availableKernelModules = [ "nvme" "xhci_pci" "thunderbolt" "usb_storage" "sd_mod" ];
      removableEfi = false;
      kernelParams = [ ];
      sshUnlock = {
        # read sshUnlock.txt file.
        enable = false;
        authorizedKeys = [ ];
      };
      disableSwap = true; # if true, the swap partition does not have to exist
    };
    networking = {
      # read changeHostName.txt file.
      hostName = "the-world";
      timeZone = "Europe/Zurich";
      hostId = "3270ece1";
    };
  };

  workstation.enable = true; # NB:
  # - False implies a minimal selection of programs that is ideal for servers.
  # - Set to true if the device is an end user device, e.g., a laptop.
}
