{ config, lib, pkgs, ... }: {
  imports = [ ./boot ./file-systems ./networking ./workstation ];
}
