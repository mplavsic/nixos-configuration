{ config, lib, pkgs, userName, userDescription, home-manager, stateVersion, ... }:
let inherit (lib) types mkIf mkDefault mkOption;
in {
  options.workstation = {
    enable = mkOption {
      description = "Supposed to be enabled on end-user devices, such as laptops or PCs. It comes with all needed packages.";
      type = types.bool;
      default = false;
    };
  };

  # if workstation.enable is set to true set the following options:
  config = mkIf config.workstation.enable {

    # -------------------------------------------------------
    # HOME MANAGER
    # -------------------------------------------------------

    home-manager.useGlobalPkgs = true;
    home-manager.useUserPackages = true;
    home-manager.users.${userName} = (import ./home-manager.nix);
    home-manager.extraSpecialArgs = {stateVersion = stateVersion; userName = userName;};
    # Optionally, use home-manager.extraSpecialArgs to pass arguments to home-manager.nix

    # -------------------------------------------------------
    # FILESYSTEMS, HARDWARE, SCANNERS/PRINTERS AND NETWORKING
    # -------------------------------------------------------

    boot.supportedFilesystems = [ "ntfs" "fat32" ];

    services.fwupd.enable = true;

    # Enable sound with pipewire.
    sound.enable = false; # this conflics with pipewire
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };

    hardware.bluetooth = {
      enable = true;
      settings = {
        General = {
          ControllerMode = "dual";
          FastConnectable = "true";
          Experimental = "true";
        };
        Policy = {
          AutoEnable = "true";
        };
      };
    };

    # For printers
    services.printing.enable = true;
    # NB: do not enable these drivers, as CUPS should be enough.
    # services.printing.drivers = [ pkgs.dcp9020cdwlpr pkgs.dcp9020cdw-cupswrapper];

    #! NB: do not enable avahi, as it does not always work correctly...
    # services.avahi.enable = true;
    # services.avahi.nssmdns = true;
    # for a WiFi printer
    # services.avahi.openFirewall = true;

    #! NB: instead of avahi, work directly with a static IP address over IPP. Example:
    # Name/Description: DCP-9015CDW
    # Driver:	Generic PCL Laser Printer (grayscale, 2-sided printing)
    # Connection: ipp://192.168.0.195/ipp
    #! NB: prefer IPP over AppSocket (socket://192.168.0.195:9100) or LPD,
    #! as it is newer, allows to choose color/bw mode, shows accurate printing status, etc...

    # Scanners
    hardware.sane = {
      enable = true;
      brscan4 = {
        enable = true;
        netDevices = {
          #! NB: setting a static IP to the printer makes it easy to add a scanner (in case of same device)
          home = { model = "DCP-9015CDW"; ip = "192.168.178.107"; };
        };
      };
    };
    # NB: make sure you add the user to the "scanner" and "lp" group 

    services.openssh = {
      enable = false;
    };

    # -------------------------------------------------------
    # VIRTUALIZATION
    # -------------------------------------------------------

    virtualisation = {
      podman = {
        enable = true;
        # Create a `docker` alias for podman, to use it as a drop-in replacement
        dockerCompat = true;
        # Required for containers under podman-compose to be able to talk to each other.
        defaultNetwork.settings.dns_enabled.enable = true;
        extraPackages = [ pkgs.zfs ];
      };
      oci-containers.backend = "podman";
      containers.storage.settings = {
        storage = {
          driver = "zfs";
          graphroot = "/var/lib/containers/storage";
          runroot = "/run/containers/storage";
        };
      };
      # you can opt to disable podman in favour of docker:
      # docker = {
      #   enable = true;
      #   storageDriver = "zfs";
      # };
      #! NB: to download image: doas waydroid init -s GAPPS -c https://ota.waydro.id/system -v https://ota.waydro.id/vendor -f
      waydroid.enable = true;
      libvirtd = {
        enable = true;
      };
    };

    # Should the waydroid simulator not have access to the internet,
    # uncommenting the following lines should help:
    # networking.firewall.trustedInterfaces = [
    #   "waydroid0"
    # ];
    # networking.firewall.allowedTCPPorts = [
    #   53
    #   67
    # ];
    # networking.firewall.allowedUDPPorts = [
    #   53
    #   67
    # ];
    #! NB: the above line are probably not necessary: traffic needs to be forwarded to dnscrypt-proxy2 for internet to work
    
    # -------------------------------------------------------
    # USERS AND SECURITY
    # -------------------------------------------------------

    users.users = {
      ${userName} = {
        extraGroups = [
          "scanner"
          "lp"
          "docker"
          "kvm" # for the official Android emulator
          "libvirtd" # for the official Android emulator
          # "qemu-libvirtd" # for the official Android emulator (should not be needed)
          "adbusers"
        ];
      };
    };

    # In case sudo is needed, uncomment the following lines:
    # security =  {
    #   sudo.enable = true;
    # };

    # -------------------------------------------------------
    # ENVIRONMENTS AND PROGRAMS
    # -------------------------------------------------------

    services.xserver = {
      enable = true;
      desktopManager.gnome.enable = true;
      displayManager.gdm.enable = true;
      excludePackages = [ pkgs.xterm ];
    };

    systemd.tmpfiles.rules = let userMonitorXmlPath = /home/${userName}/.config/monitors.xml; in
      if (builtins.pathExists userMonitorXmlPath) then
        let
          monitorsXmlContent = builtins.readFile userMonitorXmlPath;
          monitorsConfig = pkgs.writeText "gdm_monitors.xml" monitorsXmlContent;
        in
          [ "L+ /run/gdm/.config/monitors.xml - - - - ${monitorsConfig}" ]
      else
        []
      ;

    environment.sessionVariables = {
      QT_QPA_PLATFORM="wayland";
      NIXOS_OZONE_WL="1";
      MOZ_ENABLE_WAYLAND="1"; # Firefox, Tor Browser, ...
      ANKI_WAYLAND="1";
    };

    programs.adb.enable = true;

    services.flatpak.enable = true;

    environment.gnome.excludePackages = builtins.attrValues {
      inherit (pkgs)
        gnome-console # use blackbox instead
        gnome-photos
        gnome-tour
        gedit # text editor
      ;

      inherit (pkgs.gnome)
        # evince # document viewer
        # gnome-characters # installed fonts browser
        # gnome-disk-utility # Use nix shell for this
        atomix # puzzle game
        cheese # webcam tool
        epiphany # web browser
        geary # email reader
        gnome-maps
        gnome-music
        gnome-terminal # use blackbox instead
        hitori # sudoku game
        iagno # go game
        tali # poker game
        totem # video player
      ;
    };

    environment.systemPackages = builtins.attrValues {
      inherit (pkgs.gnome)
        dconf-editor
        eog
        gnome-session
        gnome-tweaks
        nautilus-python # to add "open in blackbox" option in context menu
      ;

      inherit (pkgs.xorg)
        xeyes
      ;

      inherit (pkgs.texlive.combined)
        scheme-tetex 
      ;
      
      inherit (pkgs)
        # GNOME software (official and unofficial) and GTK-first software:
        blackbox-terminal
        evolution # needed for setting up CalDAV and CardDAV integration (with GNOME's Calendar and Contacts)
        gnome-browser-connector
        nautilus-open-any-terminal # to add "open in blackbox" option in context menu (either install it from here or using "nix-env -iA", but not both)
        shortwave

        # Miscellaneous:
        anki-bin
        # apacheHttpd
        bitwarden
        distrobox
        dbeaver
        # docker-compose
        ffmpeg
        firefox
        gimp-with-plugins
        insomnia
        libheif # needed in combination with pkgs.gnome.eog for iPhone pics
        libreoffice-still
        libva-utils
        lollypop
        marktext
        mpv
        nodejs
        pdfstudio2022 # tag:unfree
        # podman-compose # commented out due to docker usage
        qbittorrent
        remmina
        restic
        sanoid
        signal-desktop
        sqlitebrowser
        standardnotes
        tdesktop
        texstudio
        thunderbird
        tor-browser-bundle-bin
        vscodium
        wl-clipboard
      ;
    };

    # -------------------------------------------------------
    # GNOME AUTOLOGIN
    # -------------------------------------------------------

    # Enable automatic login for the user.
    services.xserver.displayManager.autoLogin.enable = true;
    services.xserver.displayManager.autoLogin.user = userName;

    # NB:
    # - DO NOT SET: services.gnome.gnome-keyring.enable = false;
    #   Reason: some apps need the keyring.
    # - Instead, check the README.md to for a workaround to
    #   unlock the keyring and keep GNOME autologin enabled.

    # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
    # NB: This configs ought to be set in the end for it to work.
    systemd.services."getty@tty1".enable = false;
    systemd.services."autovt@tty1".enable = false;

    # -------------------------------------------------------
    # FONTS
    # -------------------------------------------------------

    fonts.packages = with pkgs;
      [
        fira-code # monospace font w/ ligatures
        fira-code-symbols
        fira-mono # monospace font
        liberation_ttf
        (nerdfonts.override { fonts = [ "DroidSansMono" "FiraCode" ]; })
        noto-fonts
        noto-fonts-cjk
        noto-fonts-emoji
      ];
  };
}
