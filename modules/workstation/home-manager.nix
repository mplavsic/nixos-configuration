# NB: Home manager is enabled only if `workspace.enable = true`.
{lib, stateVersion, userName, ... }:
{
    dconf.enable = true;
    dconf.settings = with lib.hm.gvariant; {
      "org/gnome/desktop/background" = {
        color-shading-type = "solid";
        picture-options = "zoom";
        picture-uri = ""; # if you want to use a file: "file:///home/manuel/Pictures/Wallpapers/hello-world.svg";
        picture-uri-dark = ""; # if you want to use a file: "file:///home/manuel/Pictures/Wallpapers/hello-world.svg";
        primary-color = "#6e6e6e";
        secondary-color = "#000000";
      };
      "org/gnome/desktop/media-handling" = {        
        automount = false;
        automount-open = false;
      };
      "org/gnome/desktop/peripherals/mouse" = {
        natural-scroll = true;
      };
      "org/gnome/desktop/peripherals/touchpad" = {
        tap-to-click = true;
      };
      "org/gnome/shell/window-switcher" = {
        current-workspace-only = false;
      };
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        # some apps don't support "color-scheme", therefore the following config is still necessary
        # (source: https://www.reddit.com/r/gnome/comments/tzs1r4/comment/i40yjzh/?utm_source=share&utm_medium=web3x):
        gtk-theme = "Adwaita-dark";
        show-battery-percentage = true;
        font-antialising = "rgba"; # for LCD displays
      };
      "org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "nothing"; # power button powers off machine (other options: 'suspend', 'nothing')
        # power-saver-profile-on-low-battery = false;
        sleep-inactive-ac-timeout = 1200;
        sleep-inactive-ac-type = "blank"; # nothing means "don't suspend"
        sleep-inactive-battery-timeout = 900;
        sleep-inactive-battery-type = "blank"; # nothing means "don't suspend"
      };

      # gnome keybindings
      # for keybinding examples visit https://the-empire.systems/nixos-gnome-settings-and-keyboard-shortcuts
      
      "org/gnome/desktop/wm/keybindings" = {
        panel-run-dialog = ["<Super>r"]; # instead of Alt+F2
        switch-applications = ["<Alt>Tab"];
        switch-applications-backward = ["<Shift><Alt>Tab"];
        switch-windows = ["<Super>Tab"];
        switch-windows-backward = ["<Shift><Super>Tab"];
      };

      # gnome dash
      "org/gnome/shell" = {
        favorite-apps = ["org.gnome.Nautilus.desktop" "gnome-system-monitor.desktop" "com.raggesilver.BlackBox.desktop" "codium.desktop" "sqlitebrowser.desktop" "insomnia.desktop" "thunderbird.desktop" "firefox.desktop" "signal.desktop" "org.gnome.Lollypop.desktop" "anki.desktop" "standard-notes.desktop" "org.gnome.TextEditor.desktop" "bitwarden.desktop"];
      };

      # gnome text editor
      "org/gnome/TextEditor" = {
        custom-font = "FiraCode Nerd Font Mono weight=450 11";
        discover-settings = false;
        restore-session = false;
        style-scheme = "builder-dark";
        use-system-font = false;
      };

      # black box
      "com/raggesilver/BlackBox" = { 
        fill-tabs = false;
        font = "FiraCode Nerd Font Mono 12";
        headerbar-drag-area = true;
        headerbar-draw-line-single-tab = false;
        scrollback-mode = lib.hm.gvariant.mkUint32 0;
        scrollback-lines = lib.hm.gvariant.mkUint32 100000;
        show-headerbar = true;
        style-preference = lib.hm.gvariant.mkUint32 2;
        theme-dark = "Monokai Dark"; # on helix set matching theme: "monokai_pro_spectrum"
      };

      # black box integration
      "com/github/stunkymonkey/nautilus-open-any-terminal" = {
        terminal = "blackbox";
        keybindings = "<Alt>t";
        new-tab = true;
      };

      # -------------------------------------------------------
      # EXTENSIONS
      # -------------------------------------------------------

      "org/gnome/shell/extensions/sane-airplane-mode" = {
        enable-airplane-mode = false;
        enable-bluetooth = false;
      };
      
      "org/gnome/shell/extensions/bluetooth-quick-connect" = {        
        refresh-button-on = true;
        show-battery-value-on = true;
      };

      "org/gnome/shell/extensions/just-perfection" = { 
        accessibility-menu = false;
        activities-button = false;
        animation = 3;
        app-menu = true;
        # "clock-menu-position" = 2;
        window-demands-attention-focus = true;
        workspace-switcher-should-show = true;
      };
    };
    home.file = {
      ".config/" = {
        source = ./dotfiles/.config;
        recursive = true;
      };
      ".local/" = {
        source = ./dotfiles/.local;
        recursive = true;
      };
    };
    home.stateVersion = stateVersion;
}
